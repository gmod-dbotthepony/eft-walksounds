
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local EFT_ENABLE_SV = DLib.util.CreateSharedConvar('sv_eft_footsteps_enable', '1', 'Enable Escape From Tarkov footsteps serverside')
local EFT_ENABLE_LANDING_SV = DLib.util.CreateSharedConvar('sv_eft_footsteps_landing', '1', 'Enable landing sounds serverside')
local EFT_ENABLE_JUMPING_SV = DLib.util.CreateSharedConvar('sv_eft_footsteps_jumping', '1', 'Enable jumping sounds serverside')
local EFT_ENABLE_TURN_SV = DLib.util.CreateSharedConvar('sv_eft_footsteps_turning', '1', 'Enable turning sounds serverside')
local EFT_ENABLE_STOP_SV = DLib.util.CreateSharedConvar('sv_eft_footsteps_stopping', '1', 'Enable stopping sounds serverside')
local EFT_ENABLE_GEAR_SV = DLib.util.CreateSharedConvar('sv_eft_footsteps_gear', '1', 'Enable gear sounds serverside')
local EFT_ENABLE, EFT_ENABLE_LANDING, EFT_ENABLE_JUMPING, EFT_ENABLE_TURN, EFT_ENABLE_STOP, EFT_ENABLE_GEAR

if SERVER then
	AddCSLuaFile('eft_registry.lua')
	net.pool('eftwalksound')
	resource.AddWorkshop('2230307188')
else
	EFT_ENABLE = CreateConVar('cl_eft_footsteps_enable', '1', {FCVAR_ARCHIVE}, 'Enable Escape From Tarkov footsteps for yourself')
	EFT_ENABLE_LANDING = CreateConVar('cl_eft_footsteps_landing', '1', {FCVAR_ARCHIVE}, 'Enable landing sounds for yourself')
	EFT_ENABLE_JUMPING = CreateConVar('cl_eft_footsteps_jumping', '1', {FCVAR_ARCHIVE}, 'Enable jumping sounds for yourself')
	EFT_ENABLE_TURN = CreateConVar('cl_eft_footsteps_turning', '1', {FCVAR_ARCHIVE}, 'Enable turning sounds for yourself')
	EFT_ENABLE_STOP = CreateConVar('cl_eft_footsteps_stopping', '1', {FCVAR_ARCHIVE}, 'Enable stopping sounds for yourself')
	EFT_ENABLE_GEAR = CreateConVar('cl_eft_footsteps_gear', '1', {FCVAR_ARCHIVE}, 'Enable gear step sounds for yourself')
end

local soundpool = {}
local soundpool_rev = {}

local function LEmitSound(self, path, level, volume)
	if CLIENT then
		if not game.SinglePlayer() then self:EmitSound(path, level or 75, 100, volume) end
		return
	end

	if game.SinglePlayer() then
		self:EmitSound(path, level or 75, 100, volume)
		return
	end

	if not soundpool[path] then
		error('Playing unpooled sound: ' .. path)
	end

	local filter = RecipientFilter()
	filter:AddPAS(self:GetPos())
	filter:RemovePlayer(self)

	if filter:GetCount() == 0 then return end

	net.Start('eftwalksound', true)
	net.WritePlayer(self)
	net.WriteUInt16(soundpool[path])
	net.WriteUInt8(level or 75)
	net.WriteUInt8(math.floor((volume or 1) * 100))
	net.Send(filter)
end

if CLIENT then
	net.receive('eftwalksound', function()
		local ply = net.ReadPlayer()
		local sound = assert(soundpool_rev[net.ReadUInt16()], 'sound not found')
		local level = net.ReadUInt8()
		local volume = net.ReadUInt8() / 100

		if IsValid(ply) then
			ply:EmitSound(sound, level, 100, volume)
		end
	end)
end

local registry = include('eft_registry.lua')

local next = 0

for _, sub in pairs(registry) do
	for mat, list in pairs(sub) do
		for index, sound in pairs(list) do
			soundpool[sound] = next
			soundpool_rev[next] = sound
			next = next + 1
		end
	end
end

local vec1 = Vector(0, 0, 15)
local vec2 = Vector(0, 0, 5)

local function gettrace(self, dropToGround)
	local mins, maxs = self:GetHull()

	local trData = {
		start = self:GetPos(),
		endpos = self:GetPos() - (dropToGround and vec1 or vec2),
		mins = mins, maxs = maxs,
		filter = self
	}

	return util.TraceHull(trData)
end

local function PlayerFootstep(self, pos, foot, sound, volume, filter)
	if self.IsPony and self:IsPony() then return end
	if not EFT_ENABLE_SV:GetBool() then return end
	if EFT_ENABLE and self == LocalPlayer() and not EFT_ENABLE:GetBool() then return end
	if not self:Alive() then return end

	local vel = self:GetVelocity():Length()
	local runspeed = self:GetWalkSpeed():min(self:GetRunSpeed() * 1.1)

	if self:GetActiveWeapon():IsValid() and self:GetActiveWeapon().IsTFAWeapon then
		runspeed = runspeed * self:GetActiveWeapon():GetStat('MoveSpeed', 1)
	end

	local isrunning = vel >= runspeed * 1.1
	local iswalking = vel < runspeed * 0.9
	local waterlevel = self:WaterLevel()

	local tr = gettrace(self, false)
	local matType = tr.MatType == 0 and MAT_DEFAULT or tr.MatType
	local surfName = tr.SurfaceProps == 0 and 'default' or util.GetSurfacePropName(tr.SurfaceProps)

	if self:GetMoveType() == MOVETYPE_LADDER then
		matType = MAT_GRATE
	end

	if waterlevel >= 1 and waterlevel ~= 3 then
		local splashreg = registry[isrunning and 'sprint' or (iswalking and 'walk' or 'run')][MAT_SLOSH]
		local splashsound = splashreg[math.random(1, #splashreg)]

		if splashsound then
			LEmitSound(self, splashsound)
		end
	end

	local usegear, usereg

	if isrunning then
		usegear = EFT_ENABLE_GEAR_SV:GetBool() and (SERVER or self ~= LocalPlayer() or EFT_ENABLE_GEAR:GetBool()) and registry.gearfast[1]
		usereg = registry.sprint[surfName] or registry.sprint[matType]
	else
		usegear = EFT_ENABLE_GEAR_SV:GetBool() and (SERVER or self ~= LocalPlayer() or EFT_ENABLE_GEAR:GetBool()) and registry.gear[1]
		if iswalking then
			usereg = registry.walk[surfName] or registry.walk[matType]
		else
			usereg = registry.run[surfName] or registry.run[matType]
		end
	end

	if usegear then
		local gsound = usegear[math.random(1, #usegear)]

		if gsound then
			LEmitSound(self, gsound, nil, 0.6)
		end
	end

	if not usereg then return end
	local sound = usereg[math.random(1, #usereg)]
	if not sound then return end

	LEmitSound(self, sound)

	return true
end

local function FinishMove(self, move)
	if not IsFirstTimePredicted() then return end
	if not EFT_ENABLE_SV:GetBool() then return end
	if EFT_ENABLE and self == LocalPlayer() and not EFT_ENABLE:GetBool() then return end
	if self.IsPony and self:IsPony() then return end
	if not self:Alive() then return end

	local fwd = move:GetForwardSpeed():abs()
	local side = move:GetSideSpeed():abs()
	local velvec = self:GetVelocity()
	--local yaw = velvec:Angle().y
	local vel = velvec:Length()
	local ground = self:OnGround() and self:GetMoveType() == MOVETYPE_WALK

	local self2 = self:GetTable()
	local waterlevel = self:WaterLevel()

	--[[self2.__eft_yaw = math.clamp((self2.__eft_yaw or 0) - FrameTime() * 128 + math.AngleDifference(self2.__eft_pyaw or 0, yaw):abs(), 0, 180)
	self2.__eft_pyaw = yaw

	if self2.__eft_yaw > 45 and not self.__eft_turn then

	elseif self2.__eft_yaw <= 45 and self.__eft_turn then
		self2.__eft_yaw = false
	end]]

	if EFT_ENABLE_LANDING_SV:GetBool() and (not EFT_ENABLE_LANDING or self ~= LocalPlayer() or EFT_ENABLE_LANDING:GetBool()) then
		if ground and self.__eft_land then
			self.__eft_land = false
			local tr = gettrace(self, false)
			local matType = tr.MatType == 0 and MAT_DEFAULT or tr.MatType
			local surfName = tr.SurfaceProps == 0 and 'default' or util.GetSurfacePropName(tr.SurfaceProps)

			if waterleven ~= 3 then
				if waterlevel >= 1 then
					local sreg = registry.landing[MAT_SLOSH]

					if sreg and #sreg ~= 0 then
						LEmitSound(self, sreg[math.random(1, #sreg)])
					end
				end

				local reg = registry.landing[surfName] or registry.landing[matType]

				if reg and #reg ~= 0 then
					LEmitSound(self, reg[math.random(1, #reg)])
				end
			end
		elseif not ground and velvec.z <= -100 then
			self.__eft_land = true
		end
	end

	if EFT_ENABLE_JUMPING_SV:GetBool() and (not EFT_ENABLE_JUMPING or self ~= LocalPlayer() or EFT_ENABLE_JUMPING:GetBool()) then
		if move:KeyDown(IN_JUMP) and not self2.__eft_jump and not ground then
			self2.__eft_jump = true
			local tr = gettrace(self, true)
			local matType = tr.MatType == 0 and MAT_DEFAULT or tr.MatType
			local surfName = tr.SurfaceProps == 0 and 'default' or util.GetSurfacePropName(tr.SurfaceProps)

			if waterlevel >= 1 and waterlevel ~= 3 then
				local sreg = registry.jump[MAT_SLOSH]

				if sreg and #sreg ~= 0 then
					LEmitSound(self, sreg[math.random(1, #sreg)])
				end
			end

			local reg = registry.jump[surfName] or registry.jump[matType]

			if reg and #reg ~= 0 and waterlevel == 0 then
				LEmitSound(self, reg[math.random(1, #reg)])
			end
		elseif not move:KeyDown(IN_JUMP) and self2.__eft_jump and ground then
			self2.__eft_jump = false
		end
	end

	if EFT_ENABLE_TURN_SV:GetBool() and (SERVER or self ~= LocalPlayer() or EFT_ENABLE_TURN:GetBool()) then
		local ang = move:GetAngles()
		local head = self:GetPoseParameter('head_yaw') * 150 + 90
		local yaw = math.floor((ang.y - head) / 45)

		if yaw == -9 or yaw == -8 then
			yaw = 0
		end

		self2.__eft_turn = self2.__eft_turn or yaw

		if ground and self2.__eft_turn ~= yaw then
			local tr = gettrace(self, false)
			local matType = tr.MatType == 0 and MAT_DEFAULT or tr.MatType
			local surfName = tr.SurfaceProps == 0 and 'default' or util.GetSurfacePropName(tr.SurfaceProps)

			if waterlevel ~= 3 then
				if waterlevel >= 1 then
					local sreg = registry.turn[MAT_SLOSH]

					if sreg and #sreg ~= 0 then
						LEmitSound(self, sreg[math.random(1, #sreg)], 45, 0.1)
					end
				end

				local reg = registry.turn[surfName] or registry.turn[matType]

				if reg and #reg ~= 0 then
					LEmitSound(self, reg[math.random(1, #reg)], 45, 0.1)
				end
			end
		end

		self2.__eft_turn = yaw
	end

	self2.__eft_velocity = self2.__eft_velocity or 0

	if ground then
		if self2.__eft_velocity < vel then
			self2.__eft_velocity = Lerp(FrameTime() * 66, self2.__eft_velocity, vel)
		elseif vel < 10 then
			self2.__eft_velocity = Lerp(FrameTime() * 177, self2.__eft_velocity, vel)
		else
			self2.__eft_velocity = Lerp(FrameTime(), self2.__eft_velocity, vel)
		end
	else
		self2.__eft_velocity = 0
	end

	if EFT_ENABLE_STOP_SV:GetBool() and (SERVER or self ~= LocalPlayer() or EFT_ENABLE_STOP:GetBool()) then
		if (fwd > 40 or side > 40) and vel > 70 then
			self2.__eft_walk = true
		elseif self2.__eft_walk and vel <= 70 then
			self2.__eft_walk = false

			local tr = gettrace(self, false)
			local matType = tr.MatType == 0 and MAT_DEFAULT or tr.MatType
			local surfName = tr.SurfaceProps == 0 and 'default' or util.GetSurfacePropName(tr.SurfaceProps)

			if waterlevel ~= 3 then
				local velf = self2.__eft_velocity - 140

				if velf > 0 then
					velf = velf:sqrt() / 22

					if waterlevel >= 1 then
						local sreg = registry.stop[MAT_SLOSH]

						if sreg and #sreg ~= 0 then
							LEmitSound(self, sreg[math.random(1, #sreg)], nil, velf:clamp(0, 1))
						end
					end

					local reg = registry.stop[surfName] or registry.stop[matType]

					if reg and #reg ~= 0 then
						LEmitSound(self, reg[math.random(1, #reg)], nil, velf:clamp(0, 1))
					end
				end
			end
		end
	end
end

hook.Add('PlayerFootstep', 'EFT.Footsteps', PlayerFootstep, 2)
hook.Add('FinishMove', 'EFT.Footsteps', FinishMove, 2)

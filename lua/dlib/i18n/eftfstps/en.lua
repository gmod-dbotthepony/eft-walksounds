
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

gui.eft_footsteps.menu = 'EFT Footsteps'
gui.eft_footsteps.menu_sv = 'EFT Footsteps Server'
gui.eft_footsteps.note_self = 'Note: These settings affect only\nlocal player'
gui.eft_footsteps.note_server = 'Note: These switches work only in\nsingleplayer or listen server'

gui.eft_footsteps.cvar.cl_eft_footsteps_enable = 'Main switch'
gui.eft_footsteps.cvar.cl_eft_footsteps_landing = 'Landing sounds'
gui.eft_footsteps.cvar.cl_eft_footsteps_jumping = 'Jumping sounds'
gui.eft_footsteps.cvar.cl_eft_footsteps_turning = 'Turning sounds'
gui.eft_footsteps.cvar.cl_eft_footsteps_stopping = 'Stopping sounds'
gui.eft_footsteps.cvar.cl_eft_footsteps_gear = 'Gear sounds'

gui.eft_footsteps.cvar.sv_eft_footsteps_enable = 'Main switch serverside'
gui.eft_footsteps.cvar.sv_eft_footsteps_landing = 'Landing sounds serverside'
gui.eft_footsteps.cvar.sv_eft_footsteps_jumping = 'Jumping sounds serverside'
gui.eft_footsteps.cvar.sv_eft_footsteps_turning = 'Turning sounds serverside'
gui.eft_footsteps.cvar.sv_eft_footsteps_stopping = 'Stopping sounds serverside'
gui.eft_footsteps.cvar.sv_eft_footsteps_gear = 'Gear sounds serverside'


-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

gui.eft_footsteps.menu = 'Звуки ходьбы EFT'
gui.eft_footsteps.menu_sv = 'Звуки ходьбы EFT (сервер)'
gui.eft_footsteps.note_self = 'Примечание: Данные опции влияют только на\nповедение аддона для данного игрока'
gui.eft_footsteps.note_server = 'Примечание: Данные переключатели работают\nтолько в одиночной игре или на слушающем сервере'

gui.eft_footsteps.cvar.cl_eft_footsteps_enable = 'Главный рубильник'
gui.eft_footsteps.cvar.cl_eft_footsteps_landing = 'Звуки приземления'
gui.eft_footsteps.cvar.cl_eft_footsteps_jumping = 'Звуки прыжков'
gui.eft_footsteps.cvar.cl_eft_footsteps_turning = 'Звуки поворота'
gui.eft_footsteps.cvar.cl_eft_footsteps_stopping = 'Звуки остановки'
gui.eft_footsteps.cvar.cl_eft_footsteps_gear = 'Звуки экипировки'

gui.eft_footsteps.cvar.sv_eft_footsteps_enable = 'Главный рубильник сервера'
gui.eft_footsteps.cvar.sv_eft_footsteps_landing = 'Звуки приземления на стороне сервера'
gui.eft_footsteps.cvar.sv_eft_footsteps_jumping = 'Звуки прыжков на стороне сервера'
gui.eft_footsteps.cvar.sv_eft_footsteps_turning = 'Звуки поворота на стороне сервера'
gui.eft_footsteps.cvar.sv_eft_footsteps_stopping = 'Звуки остановки на стороне сервера'
gui.eft_footsteps.cvar.sv_eft_footsteps_gear = 'Звуки экипировки на стороне сервера'
